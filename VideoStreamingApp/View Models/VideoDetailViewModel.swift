//
//  VideoDetailViewModel.swift
//  VideoStreamingApp
//
//  Created by Jansen Ducusin on 6/2/21.
//

import Foundation

struct VideoDetailViewModel{
    let video: Video
}

extension VideoDetailViewModel {
    var title: String {
        video.title
    }
    
    var description: String {
        video.description
    }
    
    var url: URL? {
        video.url
    }
}
