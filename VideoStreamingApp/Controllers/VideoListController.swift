//
//  VideoListController.swift
//  VideoStreamingApp
//
//  Created by Jansen Ducusin on 6/2/21.
//

import UIKit
import AVKit
import AVFoundation

class VideoListController: UITableViewController {
    // MARK: - Properties

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTableView()
    }

    // MARK: - Helpers
    private func setupUI(){
        view.backgroundColor = .white
        title = "Video Streaming App"
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    private func setupTableView(){
        tableView.tableFooterView = UIView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

}

// MARK: - UITableView Datasource
extension VideoListController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Video.samples.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = Video.samples[indexPath.row].title
        cell.accessoryType = .disclosureIndicator
        return cell
    }
}

// MARK: - UITableView Delegate
extension VideoListController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let video = Video.samples[indexPath.row]
        showFullScreen(video)
//        showOnDetailView(video)
    }
    
    private func showFullScreen(_ video: Video){
        guard let url = video.url else {return}
        
        let player = AVPlayer(url: url)
        let controller = AVPlayerViewController()
        controller.player = player
        
        present(controller, animated: true)
    }
    
    private func showOnDetailView(_ video: Video){
        let controller = VideoDetailController(video: video)
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true)
    }
}
