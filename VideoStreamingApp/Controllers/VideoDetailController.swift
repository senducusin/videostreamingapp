//
//  VideoDetailController.swift
//  VideoStreamingApp
//
//  Created by Jansen Ducusin on 6/2/21.
//

import UIKit

class VideoDetailController: UITableViewController {
    // MARK: - Properties
    private let viewModel: VideoDetailViewModel
    private let headerView = VideoDetailHeader(frame: .init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 250))
    
    // MARK: - Lifecycle
    init(video: Video){
        viewModel = VideoDetailViewModel(video: video)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.title
        
        headerView.delegate = self
        headerView.video = viewModel.video
        headerView.parentView = view
        
        setupTableView()
    }
    
    
    // MARK: - Helpers
    private func setupTableView(){
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
    }
}

extension VideoDetailController: VideoDetailHeaderDelegate {
    func backButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
}
