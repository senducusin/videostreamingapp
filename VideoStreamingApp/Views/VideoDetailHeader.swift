//
//  VideoDetailHeader.swift
//  VideoStreamingApp
//
//  Created by Jansen Ducusin on 6/2/21.
//

import UIKit
import AVKit
import AVFoundation

protocol VideoDetailHeaderDelegate: AnyObject {
    func backButtonPressed()
}

class VideoDetailHeader: UIView {
    // MARK: - Properties
    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(dismissHandler), for: .touchUpInside)
        button.setImage(UIImage(systemName: "chevron.down"), for: .normal)
        button.tintColor = .white
        return button
    }()
    
    var video: Video?{
        didSet{
            guard let url = video?.url else {return}
            player = AVPlayer(url: url)
            playerLayer = AVPlayerLayer(player: player)
            print("DEBUG: did set video")
        }
    }
    var parentView: UIView! {
        didSet {
            playerLayer.frame = parentView.bounds
            layer.addSublayer(playerLayer)
            
            guard let _ = video else {return}
            print("DEBUG: should play...")
            player?.play()
        }
    }
    
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer!
    
    weak var delegate: VideoDetailHeaderDelegate?
    
    // MARK: - Lifecycle
    override init(frame:CGRect){
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setupUI()

        
        playerLayer.frame = self.bounds
        layer.addSublayer(playerLayer)
        player?.play()
    }
    
    // MARK: - Selectors
    @objc private func dismissHandler(){
        stop()
        delegate?.backButtonPressed()
    }
    
    // MARK: - Helpers
    private func stop(){
        player?.pause()
        playerLayer.removeFromSuperlayer()
        player = nil
    }
    
    private func setupUI(){
        backgroundColor = .darkGray
        setupBackButton()
    }
    
    private func setupBackButton(){
        addSubview(backButton)
        backButton.anchor(top: topAnchor, left: leftAnchor, paddingTop: 10, paddingLeft: 17)
//        backButton.bringSubviewToFront(<#T##view: UIView##UIView#>)
    }
}
