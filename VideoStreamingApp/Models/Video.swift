//
//  Video.swift
//  VideoStreamingApp
//
//  Created by Jansen Ducusin on 6/2/21.
//

import Foundation

struct Video {
    let title: String
    let url: URL?
    let description: String
}

extension Video {
    static let samples = [
        Video(
            title: "Cosmos: War of the Planets",
            url: URL(string: "https://archive.org/download/Cosmos_War_of_the_Planets/Cosmos_War_of_the_Planets.mp4"),
            description: "A spaceship intercepts a mysterious message and then crashes on the planet the message is being sent from. There they uncover a force that threatens to take over the Earth."
        ),
        Video(
            title: "Night of the Living Dead",
            url: URL(string: "https://archive.org/download/night_of_the_living_dead_dvd/Night.mp4"),
            description: "I did some digital tweaking on Night of the Living Dead to make it more watchable and made it into a DVD image that you can burn."
        ),
        Video(
            title: "Voyage To The Planet Of Prehistoric Women",
            url: URL(string: "https://archive.org/download/VoyageToThePlanetOfPrehistoricWomen_20130813/Voyage%20to%20the%20Planet%20of%20Prehistoric%20Women.mp4"),
            description: "'Voyage to the Planet of the Prehistoric Women' (1968) is the second of two reworkings of 'Planeta Bur,' a Soviet sci-fi film from 1959. The first was the similarly titled, 'Voyage to the Prehistoric Planet' (1962) assembled by director Curtis Harrington for producer Roger Corman."
        ),
        Video(
            title: "Battle Of The Worlds 1961",
            url: URL(string: "https://archive.org/download/BattleOfTheWorldsWidesceen/Battle%20of%20the%20Worlds%20NTSC_1.mp4"),
            description: "Battle of the Worlds is a 1961 Italian science fiction film directed by Antonio Margheriti, starringClaude Rains, Bill Carter and Maya Brent. The Italian title translates as The Planet of Extinct Men."
        ),
        Video(
            title: "First Spaceship on Venus",
            url: URL(string: "https://archive.org/download/FirstSpaceshipOnVenusMPEG/First_Spaceship_On_Venus.mp4"),
            description: "A spaceship on a mission to Venus uncovers an increasingly sinister secret."
        )
    ]
}
